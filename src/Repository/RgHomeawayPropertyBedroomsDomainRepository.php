<?php

namespace App\Repository;

use App\Entity\Homeawayuk\RgHomeawayPropertyBedroomsDomain;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method RgHomeawayPropertyBedroomsDomain|null find($id, $lockMode = null, $lockVersion = null)
 * @method RgHomeawayPropertyBedroomsDomain|null findOneBy(array $criteria, array $orderBy = null)
 * @method RgHomeawayPropertyBedroomsDomain[]    findAll()
 * @method RgHomeawayPropertyBedroomsDomain[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class RgHomeawayPropertyBedroomsDomainRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, RgHomeawayPropertyBedroomsDomain::class);
    }

    // /**
    //  * @return RgHomeawayPropertyBedroomsDomain[] Returns an array of RgHomeawayPropertyBedroomsDomain objects
    //  */
    
    public function findByBedrooms()
    {
        return $this->createQueryBuilder('u')
            ->orderBy('u.name', 'ASC')
            ->getQuery()
            ->getArrayResult()
        ;
    }
    
}
