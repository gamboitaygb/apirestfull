<?php

namespace App\Repository;

use App\Entity\Homeawayuk\RgHomeawayBookingSeasonDomain;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method RgHomeawayBookingSeasonDomain|null find($id, $lockMode = null, $lockVersion = null)
 * @method RgHomeawayBookingSeasonDomain|null findOneBy(array $criteria, array $orderBy = null)
 * @method RgHomeawayBookingSeasonDomain[]    findAll()
 * @method RgHomeawayBookingSeasonDomain[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class RgHomeawayBookingSeasonDomainRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, RgHomeawayBookingSeasonDomain::class);
    }

    // /**
    //  * @return RgHomeawayBookingSeasonDomain[] Returns an array of RgHomeawayBookingSeasonDomain objects
    //  */
    
    public function findByBookingSeasson()
    {
        return $this->createQueryBuilder('u')
            ->orderBy('u.name', 'ASC')
            ->getQuery()
            ->getArrayResult()
        ;
    }
    
}
