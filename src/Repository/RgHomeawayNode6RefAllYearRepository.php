<?php

namespace App\Repository;

use App\Entity\Homeawayuk\RgHomeawayNode6RefAllYear;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method RgHomeawayNode6RefAllYear|null find($id, $lockMode = null, $lockVersion = null)
 * @method RgHomeawayNode6RefAllYear|null findOneBy(array $criteria, array $orderBy = null)
 * @method RgHomeawayNode6RefAllYear[]    findAll()
 * @method RgHomeawayNode6RefAllYear[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class RgHomeawayNode6RefAllYearRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, RgHomeawayNode6RefAllYear::class);
    }

    // /**
    //  * @return RgHomeawayNode6RefAllYear[] Returns an array of RgHomeawayNode6RefAllYear objects
    //  */
    
    public function findByCities()
    {
        return $this->createQueryBuilder('u')
            ->select('u.rgHomeawayNode6RefAllYearId,u.city')
            ->orderBy('u.rgHomeawayNode6RefAllYearId', 'ASC')
            ->getQuery()
            ->getArrayResult()
        ;
    }



    public function findOneById($slug)
    {
        if(!is_int((int)$slug)){
            return [];
        }
        return $this->createQueryBuilder('u')
            ->select('u.rgHomeawayNode6RefAllYearId,u.country,u.region,u.area')
            ->where('u.rgHomeawayNode6RefAllYearId = :val')
            ->setParameter('val', $slug)
            ->getQuery()
            ->getArrayResult()
        ;
    }

    public function findOneByCity($slug)
    {
        if(!is_string($slug)){
            return [];
        }
        return $this->createQueryBuilder('u')
            ->select('u.rgHomeawayNode6RefAllYearId,u.country,u.region,u.area')
            ->where('u.city = :val')
            ->setParameter('val', $slug)
            ->getQuery()
            ->getArrayResult()
        ;
    }

    public function findByCitiesAndAreas()
    {
        return $this->createQueryBuilder('u')
            ->select('u.rgHomeawayNode6RefAllYearId,u.city,u.area')
            ->orderBy('u.city,u.area', 'ASC')
            ->getQuery()
            ->getArrayResult()
        ;
    }

    

    /*
    public function findOneBySomeField($value): ?User
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
    
}
