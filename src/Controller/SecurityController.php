<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Serializer\Encoder\EncoderInterface;
use Symfony\Component\Translation\TranslatorInterface;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use App\Entity\User;

class SecurityController extends AbstractController
{
    /**
     * @Route("/", 
     * name="api",
     * methods={"POST"})
     */
    public function index()
    {
        return $this->json([
            'message' => 'Welcome to your new controller!',
            'path' => 'src/Controller/SecurityController.php',
        ]);
    }

    /**
     * @Route("/api/login",
     *     name="user_login")
     */
    public function login()
    {

    }

    /**
     * @Route("/api/logout",
     *     name="user_logout")
     */
    public function logout()
    {

    }


    /**
     * @Route("/api/signup",
     *     requirements={"_locale"="en|es|fr|it|pt"},
     *     name="create_account")
     */
    public function createAccount(Request $request,UserPasswordEncoderInterface $encoder){

        
                $em = $this->getDoctrine()->getManager();

                $user= new User();

                $encoded = $encoder->encodePassword($user,'yusmel');
                $user->setPassword($encoded);
                $user->setName('Yusmel');
                $user->setUsername('yus');
                $user->setRoles('ROLE_ADMIN');
                $user->setCreateAt( new \DateTime('now'));
                $user->updatedTimestamps();
                $user->setEmail('yus@gmail.com');

            
                $em->persist($user);
                $em->flush();

                if(\is_object($user)){
                    die("todo cool");
                }

                return false;

        }
}
