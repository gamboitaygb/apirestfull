<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use App\Entity\Homeawayuk\RgHomeawayNode6RefAllYear;
use App\Entity\Homeawayuk\RgHomeawayPropertyBedroomsDomain;
use App\Entity\Homeawayuk\RgHomeawayBookingSeasonDomain;


/**
 * @Route("/api/homeawayuk/v1.0")
 */
class ApiHomeAwayUkController extends AbstractController
{
    /**
     * @Route("/get_cities/", 
     * methods={"GET"},
     * name="get_cities")
     */
    public function getCities(Request $request)
    {

        $em = $this->getDoctrine()->getManager('homeaway_uk');
        $result = $em->getRepository(RgHomeawayNode6RefAllYear::class, 'homeaway_uk')->findByCities([]);

        return $this->json( $result);
    }

    /**
     * @Route("/get-cities-areas/", 
     * methods={"GET"},
     * name="get_cities_areas")
     */
    public function getCitiesAndAreas()
    {
        $em = $this->getDoctrine()->getManager('homeaway_uk');
        $result = $em->getRepository(RgHomeawayNode6RefAllYear::class, 'homeaway_uk')->findByCitiesAndAreas([]);

        return $this->json( $result);  
    }



    /**
     * @Route("/location/{id}/", 
     * methods={"GET"},
     * name="get_cities_area_id")
     */
    public function getLocationById($id)
    {
        $em = $this->getDoctrine()->getManager('homeaway_uk');
        $result = $em->getRepository(RgHomeawayNode6RefAllYear::class, 'homeaway_uk')->findOneById($id);

        return $this->json( $result,201,["content-type"=>"application/json"]);   
    }


    /**
     * @Route("/location/city/{city}/", 
     * methods={"GET"},
     * name="get_cities_area_name")
     */
    public function getLocationByCity($city)
    {
        $em = $this->getDoctrine()->getManager('homeaway_uk');
        $result = $em->getRepository(RgHomeawayNode6RefAllYear::class, 'homeaway_uk')->findOneByCity($city);

        return $this->json( $result );  
    }


    /**
     * @Route("/get-bedrooms/", 
     * methods={"GET"},
     * name="get_bedrooms")
     */
    public function getBedrooms()
    {
        $em = $this->getDoctrine()->getManager('homeaway_uk');
        $result = $em->getRepository(RgHomeawayPropertyBedroomsDomain::class, 'homeaway_uk')->findByBedrooms([]);

        return $this->json( $result);  
    }

    /**
     * @Route("/get-booking-seasson/", 
     * methods={"GET"},
     * name="get_booking_seasson")
     */
    public function getBookingSeason()
    {
        $em = $this->getDoctrine()->getManager('homeaway_uk');
        $result = $em->getRepository(RgHomeawayBookingSeasonDomain::class, 'homeaway_uk')->findByBookingSeasson([]);

        return $this->json( $result);  
    }


    /**
     * @Route("/get-location/", 
     * methods={"GET"},
     * name="get_location")
     */
    public function getLocation()
    {
        $em = $this->getDoctrine()->getManager('homeaway_uk');
        $result = $em->getRepository(RgHomeawayBookingSeasonDomain::class, 'homeaway_uk')->findByLocation([]);

        return $this->json( $result);  
    }
}
