<?php

namespace App\Entity\Homeawayuk;

use Doctrine\ORM\Mapping as ORM;

/**
 * RgHomeawayTranslate
 *
 * @ORM\Table(name="_rg_homeaway_translate", indexes={@ORM\Index(name="krama_api_rest_rg_homeaway_translate_id_translate_a6971da8", columns={"id_translate"})})
 * @ORM\Entity
 */
class RgHomeawayTranslate
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="languague_code", type="string", length=250, nullable=false)
     */
    private $languagueCode;

    /**
     * @var string
     *
     * @ORM\Column(name="country", type="string", length=250, nullable=false)
     */
    private $country;

    /**
     * @var string
     *
     * @ORM\Column(name="region", type="string", length=250, nullable=false)
     */
    private $region;

    /**
     * @var string
     *
     * @ORM\Column(name="area", type="string", length=250, nullable=false)
     */
    private $area;

    /**
     * @var string
     *
     * @ORM\Column(name="city", type="string", length=250, nullable=false)
     */
    private $city;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="timestamp", type="datetime", nullable=false)
     */
    private $timestamp;

    /**
     * @var int
     *
     * @ORM\Column(name="id_translate", type="integer", nullable=false)
     */
    private $idTranslate;


    





}
