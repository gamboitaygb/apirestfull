<?php

namespace App\Entity\Homeawayuk;

use Doctrine\ORM\Mapping as ORM;

/**
 * RgHomeawayStreaming
 *
 * @ORM\Table(name="_rg_homeaway_streaming")
 * @ORM\Entity
 */
class RgHomeawayStreaming
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var bool
     *
     * @ORM\Column(name="on", type="boolean", nullable=false)
     */
    private $on;

    /**
     * @var bool
     *
     * @ORM\Column(name="exit_loop", type="boolean", nullable=false)
     */
    private $exitLoop;


}
