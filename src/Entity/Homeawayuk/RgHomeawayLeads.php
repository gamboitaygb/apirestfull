<?php

namespace App\Entity\Homeawayuk;

use Doctrine\ORM\Mapping as ORM;

/**
 * RgHomeawayLeads
 *
 * @ORM\Table(name="_rg_homeaway_leads", indexes={@ORM\Index(name="_rg_homeaway_leads_property_bedrooms_b1f11a76", columns={"property_bedrooms"}), @ORM\Index(name="_rg_homeaway_leads_property_location_id_4098ad6c", columns={"property_location_id"}), @ORM\Index(name="_rg_homeaway_leads_property_booking_season_c23b1452", columns={"property_booking_season"})})
 * @ORM\Entity
 */
class RgHomeawayLeads
{
    /**
     * @var int
     *
     * @ORM\Column(name="rg_homeaway_leads_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $rgHomeawayLeadsId;

    /**
     * @var int|null
     *
     * @ORM\Column(name="state", type="integer", nullable=true)
     */
    private $state;

    /**
     * @var string|null
     *
     * @ORM\Column(name="firstname", type="string", length=250, nullable=true)
     */
    private $firstname;

    /**
     * @var string|null
     *
     * @ORM\Column(name="surname", type="string", length=250, nullable=true)
     */
    private $surname;

    /**
     * @var string|null
     *
     * @ORM\Column(name="phone", type="string", length=250, nullable=true)
     */
    private $phone;

    /**
     * @var string|null
     *
     * @ORM\Column(name="email", type="string", length=254, nullable=true)
     */
    private $email;

    /**
     * @var string|null
     *
     * @ORM\Column(name="property_location", type="string", length=250, nullable=true)
     */
    private $propertyLocation;

    /**
     * @var int|null
     *
     * @ORM\Column(name="robinson", type="integer", nullable=true)
     */
    private $robinson;

    /**
     * @var int|null
     *
     * @ORM\Column(name="user_id", type="integer", nullable=true)
     */
    private $userId;

    /**
     * @var int|null
     *
     * @ORM\Column(name="location_id", type="integer", nullable=true)
     */
    private $locationId;

    /**
     * @var int|null
     *
     * @ORM\Column(name="campaign_id", type="integer", nullable=true)
     */
    private $campaignId;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="timestamp", type="datetime", nullable=false)
     */
    private $timestamp;

    /**
     * @var int|null
     *
     * @ORM\Column(name="property_bedrooms", type="integer", nullable=true)
     */
    private $propertyBedrooms;

    /**
     * @var int|null
     *
     * @ORM\Column(name="property_booking_season", type="integer", nullable=true)
     */
    private $propertyBookingSeason;

    /**
     * @var int|null
     *
     * @ORM\Column(name="property_location_id", type="integer", nullable=true)
     */
    private $propertyLocationId;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="last_change", type="datetime", nullable=false)
     */
    private $lastChange;

    /**
     * @var int|null
     *
     * @ORM\Column(name="constest", type="integer", nullable=true)
     */
    private $constest;


}
