<?php

namespace App\Entity\Homeawayuk;

use Doctrine\ORM\Mapping as ORM;

/**
 * RgHomeawayResponses
 *
 * @ORM\Table(name="_rg_homeaway_responses", indexes={@ORM\Index(name="_rg_homeaway_responses_property_bedrooms_2c256c99", columns={"property_bedrooms"}), @ORM\Index(name="_rg_homeaway_responses_property_location_id_2b86fd46", columns={"property_location_id"}), @ORM\Index(name="_rg_homeaway_responses_property_location_id_request_f0446d40", columns={"property_location_id_request"}), @ORM\Index(name="_rg_homeaway_responses_property_booking_season_request_7595906e", columns={"property_booking_season_request"}), @ORM\Index(name="_rg_homeaway_responses_property_booking_season_f4a0d180", columns={"property_booking_season"}), @ORM\Index(name="_rg_homeaway_responses_rg_homeaway_leads_id_1205cbf6", columns={"rg_homeaway_leads_id"}), @ORM\Index(name="_rg_homeaway_responses_property_bedrooms_request_72387393", columns={"property_bedrooms_request"})})
 * @ORM\Entity
 */
class RgHomeawayResponses
{
    /**
     * @var int
     *
     * @ORM\Column(name="_rg_homeaway_responses_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $rgHomeawayResponsesId;

    /**
     * @var int
     *
     * @ORM\Column(name="state", type="integer", nullable=false)
     */
    private $state;

    /**
     * @var string|null
     *
     * @ORM\Column(name="property_location", type="string", length=250, nullable=true)
     */
    private $propertyLocation;

    /**
     * @var string
     *
     * @ORM\Column(name="earning_estimated", type="decimal", precision=9, scale=2, nullable=false)
     */
    private $earningEstimated;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="timestamp", type="datetime", nullable=false)
     */
    private $timestamp;

    /**
     * @var int|null
     *
     * @ORM\Column(name="property_bedrooms", type="integer", nullable=true)
     */
    private $propertyBedrooms;

    /**
     * @var int|null
     *
     * @ORM\Column(name="property_booking_season", type="integer", nullable=true)
     */
    private $propertyBookingSeason;

    /**
     * @var int|null
     *
     * @ORM\Column(name="property_location_id", type="integer", nullable=true)
     */
    private $propertyLocationId;

    /**
     * @var int
     *
     * @ORM\Column(name="rg_homeaway_leads_id", type="integer", nullable=false)
     */
    private $rgHomeawayLeadsId;

    /**
     * @var int|null
     *
     * @ORM\Column(name="property_location_id_request", type="integer", nullable=true)
     */
    private $propertyLocationIdRequest;

    /**
     * @var int|null
     *
     * @ORM\Column(name="property_bedrooms_request", type="integer", nullable=true)
     */
    private $propertyBedroomsRequest;

    /**
     * @var int|null
     *
     * @ORM\Column(name="property_booking_season_request", type="integer", nullable=true)
     */
    private $propertyBookingSeasonRequest;

    /**
     * @var string|null
     *
     * @ORM\Column(name="property_location_request", type="string", length=250, nullable=true)
     */
    private $propertyLocationRequest;


}
