<?php

/**
 *
 * Entity
 *
 * @category   Entity
 * @package    Api 
 * @author     Yus
 * @copyright  2018 https://isthrowable.com
 * @license    http://www.php.net/license/3_0.txt  PHP License 3.0
 */

namespace App\Entity\Homeawayuk;

use Doctrine\ORM\Mapping as ORM;

/**
 * RgHomeawayPropertyBedroomsDomain
 *
 * @ORM\Table(name="_rg_homeaway_property_bedrooms_domain")
 * @ORM\Entity(repositoryClass="App\Repository\RgHomeawayPropertyBedroomsDomainRepository")
 */
class RgHomeawayPropertyBedroomsDomain
{
    /**
     * @var int
     *
     * @ORM\Column(name="_rg_homeaway_property_bedrooms_domain_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $rgHomeawayPropertyBedroomsDomainId;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=250, nullable=false)
     */
    private $name;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="timestamp", type="datetime", nullable=false)
     */
    private $timestamp;


    public function getRgHomeawayPropertyBedroomsDomainId()
    {
        return $this->rgHomeawayPropertyBedroomsDomainId;
    }

    public function getName()
    {
        return $this->name;
    }

    public function getTimestamp()
    {
        return $this->timestamp;
    }


}
