<?php
/**
 *
 * @category   Entity
 * @package    Api 
 * @author     Yus
 * @copyright  2018 https://isthrowable.com
 * @license    http://www.php.net/license/3_0.txt  PHP License 3.0
 */

namespace App\Entity\Homeawayuk;

use Doctrine\ORM\Mapping as ORM;

/**
 * RgHomeawayBookingSeasonDomain
 *
 * @ORM\Table(name="_rg_homeaway_booking_season_domain")
 * @ORM\Entity(repositoryClass="App\Repository\RgHomeawayBookingSeasonDomainRepository")
 */
class RgHomeawayBookingSeasonDomain
{
    /**
     * @var int
     *
     * @ORM\Column(name="_rg_homeaway_booking_season_domain_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $rgHomeawayBookingSeasonDomainId;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=250, nullable=false)
     */
    private $name;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="timestamp", type="datetime", nullable=false)
     */
    private $timestamp;

    public function getRgHomeawayBookingSeasonDomainId()
    {
        return $this->rgHomeawayBookingSeasonDomainId;
    }

    public function getName()
    {
        return $this->name;
    }

    public function getTimestamp()
    {
        return $this->timestamp;
    }


}
