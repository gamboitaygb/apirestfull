<?php

/**
 * User.php
 *
 * User Entity
 *
 * @category   Entity
 * @package    Api 
 * @author     Yus
 * @copyright  2018 https://isthrowable.com
 * @license    http://www.php.net/license/3_0.txt  PHP License 3.0
 */

namespace App\Entity\Homeawayuk;

use Doctrine\ORM\Mapping as ORM;

/**
 * RgHomeawayNode6RefAllYear
 *
 * @ORM\Table(name="_rg_homeaway_node6_ref_all_year")
 * @ORM\Entity(repositoryClass="App\Repository\RgHomeawayNode6RefAllYearRepository")
 */
class RgHomeawayNode6RefAllYear
{
    /**
     * @var int
     *
     * @ORM\Column(name="_rg_homeaway_node6_ref_all_year_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $rgHomeawayNode6RefAllYearId;

    /**
     * @var string
     *
     * @ORM\Column(name="country", type="string", length=250, nullable=false)
     */
    private $country;

    /**
     * @var string
     *
     * @ORM\Column(name="region", type="string", length=250, nullable=false)
     */
    private $region;

    /**
     * @var string
     *
     * @ORM\Column(name="area", type="string", length=250, nullable=false)
     */
    private $area;

    /**
     * @var string
     *
     * @ORM\Column(name="city", type="string", length=250, nullable=false)
     */
    private $city;

    /**
     * @var string
     *
     * @ORM\Column(name="bedroom_1", type="decimal", precision=9, scale=2, nullable=false)
     */
    private $bedroom1;

    /**
     * @var string
     *
     * @ORM\Column(name="bedroom_2", type="decimal", precision=9, scale=2, nullable=false)
     */
    private $bedroom2;

    /**
     * @var string
     *
     * @ORM\Column(name="bedroom_3", type="decimal", precision=9, scale=2, nullable=false)
     */
    private $bedroom3;

    /**
     * @var string
     *
     * @ORM\Column(name="bedroom_4", type="decimal", precision=9, scale=2, nullable=false)
     */
    private $bedroom4;

    /**
     * @var string
     *
     * @ORM\Column(name="bedroom_5", type="decimal", precision=9, scale=2, nullable=false)
     */
    private $bedroom5;

    /**
     * @var string
     *
     * @ORM\Column(name="bedroom_6_more", type="decimal", precision=9, scale=2, nullable=false)
     */
    private $bedroom6More;

    /**
     * @var string
     *
     * @ORM\Column(name="bedroom_2_more", type="decimal", precision=9, scale=2, nullable=false)
     */
    private $bedroom2More;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="timestamp", type="datetime", nullable=false)
     */
    private $timestamp;



    public function getRgHomeawayNode6RefAllYearId()
    {
        return $this->rgHomeawayNode6RefAllYearId;
    }

    public function getCountry()
    {
        return $this->country;
    }

    public function getRegion()
    { 
        return $this->region; 
    }

    public function getArea()
    {
        return $this->area;
    }

    public function getCity()
    {
        return $this->city;
    }

    public function getBedroom1()
    {
        return $this->bedroom1;
    }

    public function getBedroom2()
    {
        return $this->bedroom2;
    }

    public function getBedroom3()
    {
        return $this->bedroom3;
    }

    public function getBedroom4()
    {
        return $this->bedroom4;
    }

    public function getBedroom5()
    {
        return $this->bedroom5;
    }

    public function getBedroom6More()
    {
        return $this->bedroom6More;
    }

    public function getBedroom2More()
    {
        return $this->bedroom2More;
    }


}
