<?php

/**
 * User.php
 *
 * User Entity
 *
 * @category   Entity
 * @package    Api 
 * @author     Yus
 * @copyright  2018 https://isthrowable.com
 * @license    http://www.php.net/license/3_0.txt  PHP License 3.0
 */

namespace App\Entity\Main;

use Doctrine\ORM\Mapping as ORM;
use DateTime;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @ORM\Table(name="user")
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 */
class User implements UserInterface
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=64)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=128, unique=true)
     */
    private $email;

    /**
     * @ORM\Column(type="string", length=64, unique=true)
     */
    private $username;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $password;

    /**
     * @ORM\Column(type="json_array")
     */
    private $roles;

    /**
     * @ORM\Column(type="datetime",name="create_at")
     */
    private $createAt;

    /**
     * @ORM\Column(type="datetime",name="update_at")
     */
    private $updateAt;


    /**
     * @ORM\Column(type="string", unique=true,name="api_token")
    */
     private $apiToken;


     /**
     * @ORM\Column(type="string", unique=true,name="api_key")
    */
    private $apiKey;



    /**
     * @ORM\Column(type="string", unique=true,name="api_secret")
    */
    private $apiSecret;




    private $plainPassword;
    private $salt;


    public function __construct()
    {
        $token = base64_encode(random_bytes(32));
        $token = strtr($token, '+/', '-_');
        $this->apiToken = $token;

        $key = base32_encode(random_bytes(20));
        $key = strtr($key, '+/', '-_');

        $this->apiKey=$key;


        $secret = base64_encode(random_bytes(20));
        $secret = strtr($secret, '+/', '-_');

        $this->apiSecret=$secret;
    }


    public function getApiToken()
    {
        return $this->apiToken;
    }


    public function getApiKey()
    {
        return $this->apiKey;
    }

    public function getApiSecret()
    {
        return $this->apiSecret;
    }


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getUsername(): ?string
    {
        return $this->username;
    }

    public function setUsername(string $username): self
    {
        $this->username = $username;

        return $this;
    }

    public function getPassword()
    {
        return $this->password;
    }

    public function setPassword(string $password)
    {
        $this->password = $password;

        return $this;
    }

    public function getPlainPassword()
    {
        return $this->plainPassword;
    }

    public function setPlainPassword(?string $plainPassword)
    {
        $this->plainPassword = $plainPassword;

        return $this;
    }



    public function getRoles()
    {
        return $this->roles;
    }

    public function setRoles($roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    public function getCreateAt(): ?\DateTimeInterface
    {
        return $this->createAt;
    }

    public function setCreateAt(\DateTimeInterface $createAt): self
    {
        $this->createAt = $createAt;

        return $this;
    }

    public function getUpdateAt(): ?\DateTimeInterface
    {
        return $this->updateAt;
    }

    public function setUpdateAt(\DateTimeInterface $updateAt): self
    {
        $this->updateAt = $updateAt;

        return $this;
    }


    public function getSalt() {}
   
    public function eraseCredentials() {}   



    /**
     * @ORM\PrePersist
     * @ORM\PreUpdate
     */
    public function updatedTimestamps()
    {
        $dateTimeNow = new DateTime('now');
        $this->setUpdateAt($dateTimeNow);
        if ($this->getCreatedAt() === null) {
            $this->setCreatedAt($dateTimeNow);
        }
    }    


    
}
